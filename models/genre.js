var mongoose  = require('mongoose');

var Schema = mongoose.Schema;

var GenreSchema = new Schema(
	{
		name: { type: String, required: true, max: 3, max: 100 }
		// category: { type: String, required: true, max: 3, max: 100 }
	}
);


//Virtual For Genres name 
// GenreSchema
// .virtual('url')
// .get(function() {
// 	return '/catalog/genre/finction' + this._id;
// });

// Virtual for Genres's URL
GenreSchema
.virtual('url')
.get(function () {
  return '/catalog/genre/' + this._id;
});


//Export model 
module.exports = mongoose.model('Genre', GenreSchema);